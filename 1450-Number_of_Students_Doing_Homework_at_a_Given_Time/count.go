package count

// Runtime: 4 ms
// Memory Usage: 2.4 MB
func busyStudent(startTime []int, endTime []int, queryTime int) int {
	count := 0
	for i, v := range endTime {
		if startTime[i] <= queryTime && queryTime <= v {
			count++
		}
	}
	return count
}
