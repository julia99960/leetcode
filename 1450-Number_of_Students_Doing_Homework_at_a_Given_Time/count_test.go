package count

import "testing"

func Test_busyStudent(t *testing.T) {
	type args struct {
		startTime []int
		endTime   []int
		queryTime int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Example 1:",
			args: args{
				[]int{1, 2, 3},
				[]int{3, 2, 7},
				4,
			},
			want: 1,
		},
		{
			name: "Example 2:",
			args: args{
				[]int{4},
				[]int{4},
				4,
			},
			want: 1,
		},
		{
			name: "Example 3:",
			args: args{
				[]int{4},
				[]int{4},
				5,
			},
			want: 0,
		},
		{
			name: "Example 4:",
			args: args{
				[]int{1, 1, 1, 1},
				[]int{1, 3, 2, 4},
				7,
			},
			want: 0,
		},
		{
			name: "Example 5:",
			args: args{
				[]int{9, 8, 7, 6, 5, 4, 3, 2, 1},
				[]int{10, 10, 10, 10, 10, 10, 10, 10, 10},
				5,
			},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := busyStudent(tt.args.startTime, tt.args.endTime, tt.args.queryTime); got != tt.want {
				t.Errorf("busyStudent() = %v, want %v", got, tt.want)
			}
		})
	}
}
