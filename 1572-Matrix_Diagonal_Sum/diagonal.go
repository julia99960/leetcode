package diagonal

func diagonalSum(mat [][]int) int {
	count := 0
	long := len(mat) - 1
	for i, v := range mat {
		count += v[i] + v[long-i]
	}
	if long%2 == 0 {
		count -= mat[long/2][long/2]
	}
	return count
}
