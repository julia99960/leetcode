package linear

// Runtime: 0 ms
// Memory Usage: 3.1 MB
func finalPrices(prices []int) []int {
	long := len(prices)
	for i, v := range prices {
		for j := i + 1; j < long; j++ {
			if v >= prices[j] {
				prices[i] = v - prices[j]
				break
			}
			if j == long-1 && v < prices[long-1] {
				prices[i] = v
			}
		}
	}
	return prices
}
