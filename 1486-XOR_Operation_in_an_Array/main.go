package main

func xorOperation(n int, start int) int {
	var output int
	for i := 0; i < n; i++ {
		output ^= start + 2*i
	}
	return output
}

func main() {
	n := 4
	start := 3
	xorOperation(n, start)
}
