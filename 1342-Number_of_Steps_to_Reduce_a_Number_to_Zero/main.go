package main

import "fmt"

func numberOfSteps(num int) int {
	c := 0
	for num > 0 {
		if num%2 == 0 { //num is even
			num = num / 2
			c++
		} else { // num is odd
			num = num - 1
			c++
		}
	}
	return c
}
func main() {
	num := 123
	ans := numberOfSteps(num)
	fmt.Println(ans)
}
