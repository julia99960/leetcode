package kids

// 103 / 103 test cases passed.
// Runtime: 4 ms
// Memory Usage: 2.4 MB
func kidsWithCandies(candies []int, extraCandies int) []bool {
	max := 0
	for _, e := range candies {
		if e >= max {
			max = e
		}
	}
	ans := make([]bool, len(candies))
	for index, element := range candies {
		if element+extraCandies >= max {
			ans[index] = true
		} else {
			ans[index] = false
		}
	}
	return ans
}
