package sum

// 53 / 53 test cases passed.
// Runtime: 4 ms
// Memory Usage: 2.7 MB
func runningSum(nums []int) []int {
	for i := 1; i < len(nums); i++ {
		nums[i] += nums[i-1]
	}
	return nums
}
