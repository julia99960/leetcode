package main

import "fmt"

func subtractProductAndSum(n int) int {
	s := []int{n}
	for n/10 > 0 {
		s = append(s, n/10)
		n = n / 10
	}

	a := 1
	b := 0
	c := 0
	for i := range s {
		if i == len(s)-1 {
			c = s[i]
			a *= c
			b += c
			break
		}
		c = s[i] - 10*(s[i+1])
		a *= c
		b += c
	}

	return a - b
	// Product of digits = 2 * 3 * 4 = 24
	// Sum of digits = 2 + 3 + 4 = 9
	// Result = 24 - 9 = 15
}

func main() {
	n := 234
	// subtractProductAndSum(n)
	ans := subtractProductAndSum(n)
	fmt.Println(ans)
}
