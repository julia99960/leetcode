package minimum

// 122 / 122 test cases passed.
// Runtime: 4 ms
// Memory Usage: 3.7 MB

// 計算最短距離的走路時間
func minTimeToVisitAllPoints(points [][]int) int {
	step := 0
	max := 0
	for i := 1; i < len(points); i++ {
		x := intAbs(points[i][0] - points[i-1][0])
		y := intAbs(points[i][1] - points[i-1][1])
		if x >= y {
			max = x
		} else {
			max = y
		}
		step += max
	}
	return step
}

// 整數取絕對值
func intAbs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
