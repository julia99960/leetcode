package count

import "testing"

func Test_countGoodTriplets(t *testing.T) {
	type args struct {
		arr []int
		a   int
		b   int
		c   int
	}

	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Example 1:",
			args{
				[]int{3, 0, 1, 1, 9, 7},
				7,
				2,
				3,
			},
			4,
		},
		{
			"Example 2:",
			args{
				[]int{1, 1, 2, 2, 3},
				0,
				0,
				1,
			},
			0,
		},
		{
			"Example 3:",
			args{
				[]int{1, 1},
				0,
				0,
				1,
			},
			0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := countGoodTriplets(tt.args.arr, tt.args.a, tt.args.b, tt.args.c); got != tt.want {
				t.Errorf("countGoodTriplets() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_intAbs(t *testing.T) {
	type args struct {
		a int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := intAbs(tt.args.a); got != tt.want {
				t.Errorf("intAbs() = %v, want %v", got, tt.want)
			}
		})
	}
}
