package project

import "testing"

func Test_projectionArea(t *testing.T) {
	type args struct {
		grid [][]int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "example 1:",
			args: args{
				[][]int{{1, 2}, {3, 4}},
			},
			want: 17,
		},
		{
			name: "example 2:",
			args: args{
				[][]int{{2}},
			},
			want: 5,
		},
		{
			name: "example 3:",
			args: args{
				[][]int{{1, 0}, {0, 2}},
			},
			want: 8,
		},
		{
			name: "example 4:",
			args: args{
				[][]int{{1, 1, 1}, {1, 0, 1}, {1, 1, 1}},
			},
			want: 14,
		},
		{
			name: "example 5:",
			args: args{
				[][]int{{2, 2, 2}, {2, 1, 2}, {2, 2, 2}},
			},
			want: 21,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := projectionArea(tt.args.grid); got != tt.want {
				t.Errorf("projectionArea() = %v, want %v", got, tt.want)
			}
		})
	}
}
