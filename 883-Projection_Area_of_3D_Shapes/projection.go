package project

// Runtime: 4 ms
// Memory Usage: 3.7 MB
func projectionArea(grid [][]int) int {
	n := len(grid)
	area := 0
	for i := 0; i < n; i++ {
		maxRow := 0
		maxCol := 0
		for j := 0; j < n; j++ {
			if grid[i][j] > 0 {
				area++
			}
			if grid[i][j] >= maxRow {
				maxRow = grid[i][j]
			}
			if grid[j][i] >= maxCol {
				maxCol = grid[j][i]
			}
		}
		area += maxRow
		area += maxCol
	}
	return area
}
