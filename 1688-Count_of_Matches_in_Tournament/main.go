package main

func numberOfMatches(n int) int {
	total := 0
	for n > 1 {
		if n%2 == 0 {
			n = n / 2
			total += n

		} else {
			n = (n - 1) / 2
			total += n + 1
		}
	}
	return total
}

func main() {
	n := 7
	numberOfMatches(n)
}
