package main

func toLowerCase(str string) string {
	output := make([]rune, len(str))
	for k, v := range str {
		if 65 <= v && v <= 90 {
			output[k] = v + 32
		} else {
			output[k] = v
		}
	}
	return string(output)
}

func main() {
	input := "Hello"
	toLowerCase(input)
}
