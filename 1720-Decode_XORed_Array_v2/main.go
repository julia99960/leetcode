package main

import "fmt"

func decode(encoded []int, first int) []int {
	ans := make([]int, len(encoded)+1)
	ans[0] = first
	for i, v := range encoded {
		ans[i+1] = ans[i] ^ v
	}
	return ans
}

func main() {
	fmt.Println(2 ^ 3)
	e := []int{6, 2, 7, 3}
	f := 4
	decode(e, f)
}
