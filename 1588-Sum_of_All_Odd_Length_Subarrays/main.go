package main

func sumOddLengthSubarrays(arr []int) int {
	get := 0

	al := len(arr)
	if al%2 == 0 {
		al = al - 1
	}

	for al > 0 {
		for i := range arr {
			for j := i; i+al <= len(arr) && j < i+al; j++ {
				get += arr[j]
			}
		}
		al -= 2
	}
	return get
}

func main() {
	arr := []int{6, 9, 14, 5, 3, 8, 7, 12, 13, 1}
	//[6,9,14,5,3,8,7,12,13,1] 878
	sumOddLengthSubarrays(arr)
}
