package main

import "fmt"

func restoreString(s string, indices []int) string {
	u := make([]byte, len(s))
	for i, n := range indices {
		u[n] = s[i]
	}
	return string(u)
}

func main() {
	s := "codeleet"
	indices := []int{4, 5, 6, 7, 0, 2, 1, 3}

	fmt.Println(restoreString(s, indices))
}
