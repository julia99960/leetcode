package main

import "fmt"

// 1512. Number of Good Pairs
// Runtime: 0 ms, faster than 100.00% of Go online submissions for Number of Good Pairs.
// Memory Usage: 2 MB, less than 16.05% of Go online submissions for Number of Good Pairs.
func numIdenticalPairs(nums []int) int {
	output := 0
	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] && i < j {
				output++
			}
		}
	}
	return output
}

func main() {
	// 1512. Number of Good Pairs
	// Given an array of integers nums.
	// A pair (i,j) is called good if nums[i] == nums[j] and i < j.
	// Return the number of good pairs.
	// Input: nums = [1,2,3,1,1,3]
	// Output: 4
	// Explanation: There are 4 good pairs (0,3), (0,4), (3,4), (2,5) 0-indexed.
	nums1512 := []int{1, 2, 3, 1, 1, 3}
	fmt.Println(numIdenticalPairs(nums1512))
}
