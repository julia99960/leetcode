package straight

// Runtime: 4 ms
// Memory Usage: 3.7 MB
func checkStraightLine(coordinates [][]int) bool {
	// 起點 (xMin,yMin) 結束點 (xMax,yMax)
	yMax := coordinates[len(coordinates)-1][1]
	yMin := coordinates[0][1]
	xMax := coordinates[len(coordinates)-1][0]
	xMin := coordinates[0][0]

	// 斜率
	var m float64

	if xMax != xMin {
		// 計算頭尾兩點之間的斜率
		m = float64(yMax-yMin) / float64(xMax-xMin)
	}

	for i := 0; i < len(coordinates); i++ {
		y := coordinates[i][1]
		x := coordinates[i][0]

		if m != 0 {
			//直線的點斜式
			if float64(y-yMax) != m*float64(x-xMax) {
				return false
			}
		} else {
			// 水平線
			if xMax != xMin && y != yMin {
				return false
			}
			// 垂直線
			if xMax == xMin && x != xMin {
				return false
			}
		}
	}
	return true
}
