package straight

import (
	"reflect"
	"testing"
)

func Test_checkStraightLine(t *testing.T) {
	type args struct {
		coordinates [][]int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Example 1:",
			args: args{
				[][]int{{1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 7}},
			},
			want: true,
		},
		{
			name: "Example 2:",
			args: args{
				[][]int{{1, 1}, {2, 2}, {3, 4}, {4, 5}, {5, 6}, {7, 7}},
			},
			want: false,
		},
		{
			name: "Example 3:",
			args: args{
				[][]int{{0, 0}, {0, 1}, {0, -1}},
			},
			want: true,
		},
		{
			name: "Example 4:",
			args: args{
				[][]int{{1, -8}, {2, -3}, {1, 2}},
			},
			want: false,
		},
		{
			name: "Example 5:",
			args: args{
				[][]int{{2, 1}, {4, 2}, {6, 3}},
			},
			want: true,
		},
		{
			name: "Example 5:",
			args: args{
				[][]int{{-3, -2}, {-1, -2}, {2, -2}, {-2, -2}, {0, -2}},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkStraightLine(tt.args.coordinates); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("checkStraightLine()= %v, want %v", got, tt.want)
			}
		})
	}
}
