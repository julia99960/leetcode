package altitude

import (
	"reflect"
	"testing"
)

func Test_largestAltitude(t *testing.T) {
	type args struct {
		gain []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Example 1:",
			args: args{
				[]int{-5, 1, 5, 0, -7},
			},
			want: 1,
		},
		{
			name: "Example 2:",
			args: args{
				[]int{-4, -3, -2, -1, 4, 3, 2},
			},
			want: 0,
		},
	}
	for _, test := range tests {
		if got := largestAltitude(test.args.gain); !reflect.DeepEqual(got, test.want) {
			t.Errorf("largestAltitude() = %v, want %v", got, test.want)
		}
	}
}
