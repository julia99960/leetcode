package altitude

// 80 / 80 test cases passed.
// Runtime: 0 ms
// Memory Usage: 2.2 MB
func largestAltitude(gain []int) int {
	altitudes := 0
	highest := 0
	for _, v := range gain {
		altitudes += v
		if highest <= altitudes {
			highest = altitudes
		}
	}
	return highest
}
