package unique

// Runtime: 0 ms
// Memory Usage: 2.5 MB
func uniqueMorseRepresentations(words []string) int {
	table := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}

	codes := make(map[string]string)
	for _, v := range words {
		var s string
		for _, k := range v {
			s += table[k-97]
		}
		codes[s] = s
	}

	return len(codes)
}
