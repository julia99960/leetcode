package ip

import (
	"strings"
)

// 62 / 62 test cases passed.
// Runtime: 0 ms
// Memory Usage: 1.9 MB
func defangIPaddr(address string) string {
	return strings.ReplaceAll(address, ".", "[.]")
}
