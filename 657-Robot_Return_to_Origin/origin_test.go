package origin

import "testing"

func Test_judgeCircle(t *testing.T) {
	type args struct {
		moves string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Example 1:",
			args: args{
				"UD",
			},
			want: true,
		},
		{
			name: "Example 2:",
			args: args{
				"LL",
			},
			want: false,
		},
		{
			name: "Example 3:",
			args: args{
				"RRDD",
			},
			want: false,
		},
		{
			name: "Example 4:",
			args: args{
				"LDRRLRUULR",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := judgeCircle(tt.args.moves); got != tt.want {
				t.Errorf("judgeCircle() = %v, want %v", got, tt.want)
			}
		})
	}
}
