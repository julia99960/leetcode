package origin

//0 ms	3.5 MB
func judgeCircle(moves string) bool {
	x, y := 0, 0
	for _, move := range moves {
		switch move {
		case 'R':
			x++
			break
		case 'L':
			x += -1
			break
		case 'U':
			y++
			break
		case 'D':
			y += -1
			break
		}
	}
	return x == 0 && y == 0
}
