package main

import "fmt"

func decompressRLElist(nums []int) []int {
	a := []int{}
	for i := range nums {
		if i%2 == 1 {
			for j := 0; j < nums[i-1]; j++ {
				a = append(a, nums[i])
			}
		}
	}
	return a
}

func main() {
	nums := []int{1, 1, 2, 3}
	s := decompressRLElist(nums)
	fmt.Println(s)
}
