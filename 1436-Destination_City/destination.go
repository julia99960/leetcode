package destination

// Runtime: 4 ms, faster than 87.50% of Go online submissions for Destination City.
// Memory Usage: 3.7 MB, less than 100.00% of Go online submissions for Destination City.
// Time complexity: O(n*n)
func destCity(paths [][]string) string {
	end := paths[0][1]
	i, c, long := 0, 0, len(paths)

	for i >= 0 && long > 1 {
		switch i {
		case long - 1: // 遍歷到尾端
			if end == paths[i][0] {
				end = paths[i][1]
			}
			if c == long {
				return end
			}
			i = 0
			c++
			break
		default: // 遍歷除了尾端的值
			if end == paths[i][0] {
				end = paths[i][1]
			}
			break
		}
		i++
	}
	return end
	/*
		// I created a hashmap to hold the starting location. I then iterate through the paths again but I'm checking whether a key exists for the destination. As soon as it's unable to find one. I return that city.
		// Time complexity: O(n)
		hashMap := make(map[string]string)
		for _, cities := range paths {
			hashMap[cities[0]] = cities[1]
		}
		for _, cities := range paths {
			if _, ok := hashMap[cities[1]]; !ok {
				return cities[1]
			}
		}
		return ""
	*/
}
