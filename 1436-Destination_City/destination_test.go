package destination

import "testing"

func Test_destCity(t *testing.T) {
	type args struct {
		paths [][]string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Example 1:",
			args: args{
				[][]string{
					{"London", "New York"},
					{"New York", "Lima"},
					{"Lima", "Sao Paulo"},
				},
			},
			want: "Sao Paulo",
		},
		{
			name: "Example 2:",
			args: args{
				[][]string{
					{"B", "C"},
					{"D", "B"},
					{"C", "A"},
				},
			},
			want: "A",
		},
		{
			name: "Example 3:",
			args: args{
				[][]string{
					{"A", "Z"},
				},
			},
			want: "Z",
		},
		{
			name: "Example 4:",
			args: args{
				[][]string{
					{"qMTSlfgZlC", "ePvzZaqLXj"},
					{"xKhZXfuBeC", "TtnllZpKKg"},
					{"ePvzZaqLXj", "sxrvXFcqgG"},
					{"sxrvXFcqgG", "xKhZXfuBeC"},
					{"TtnllZpKKg", "OAxMijOZgW"},
				},
			},
			want: "OAxMijOZgW",
		},
		{
			name: "Example 5:",
			args: args{
				[][]string{
					{"pYyNGfBYbm", "wxAscRuzOl"},
					{"wxAscRuzOl", "pYyNGfBYbm"},
				},
			},
			want: "pYyNGfBYbm", //也可以是這個答案 wxAscRuzOl
		},
		{
			name: "Example 6:",
			args: args{
				[][]string{
					{"jMgaf WaWA", "iinynVdmBz"},
					{"QCrEFBcAw", "wRPRHznLWS"},
					{"iinynVdmBz", "OoLjlLFzjz"},
					{"OoLjlLFzjz", "QCrEFBcAw"},
					{"IhxjNbDeXk", "jMgaf WaWA"},
					{"jmuAYy vgz", "IhxjNbDeXk"},
				},
			},
			want: "wRPRHznLWS",
		},
		{
			name: "Example 7:",
			args: args{
				[][]string{
					{"uOPUfreOta", "VjnupRpODE"},
					{"IQRZkZLnFO", "fALhUgpcro"},
					{"fALhUgpcro", "uOPUfreOta"},
					{"wgpXKgxyNT", "TsTjXKssvd"},
					{"akBDsWlIlI", "IQRZkZLnFO"},
					{"VjnupRpODE", "wgpXKgxyNT"},
				},
			},
			want: "TsTjXKssvd",
		},
		{
			name: "Example 8:",
			args: args{
				[][]string{
					{"ZyRLxE xmA", "WQztariTJd"},
					{"pSoBauoJox", "ZyRLxE xmA"},
					{"lbEWEqcBKg", "tKsFZosRmT"},
					{"QfKdgCRgGv", "lbEWEqcBKg"},
					{"WQztariTJd", "QfKdgCRgGv"},
					{"tKsFZosRmT", "NhNPVREEsh"},
				},
			},
			want: "NhNPVREEsh",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := destCity(tt.args.paths); got != tt.want {
				t.Errorf("destCity() = %v, want %v", got, tt.want)
			}
		})
	}
}
