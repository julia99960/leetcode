package flip

// Runtime: 0 ms
// Memory Usage: 2.8 MB
func flipAndInvertImage(A [][]int) [][]int {
	last := len(A) - 1
	for i := 0; i < len(A); i++ {
		for j := 0; j <= last/2; j++ {
			A[i][j], A[i][last-j] = 1-A[i][last-j], 1-A[i][j]
		}
	}
	return A
}
