package leaner

// O(n + m) solution
// Runtime: 20 ms
// Memory Usage: 6.5 MB
func countNegatives(grid [][]int) int {
	count := 0
	i := len(grid) - 1
	j := len(grid[0]) - 1

	for i >= 0 {
		if j < 0 {
			i--
			j = len(grid[0]) - 1
		}
		if grid[i][j] < 0 {
			count++
			j--
			if i == 0 && j == -1 {
				break
			}
		} else {
			i--
			j = len(grid[0]) - 1
		}
	}

	return count
}
