package reverse

func reverseWords(s string) string {
	reverse := ""
	invalue := ""

	for _, v := range s {
		if string(v) != " " {
			invalue = string(v)
		} else {
			long := len(invalue)
			for j := 0; j < long; j++ {
				reverse += string(invalue[long-1-j])
			}
		}
	}

	return reverse
}
