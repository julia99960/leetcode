package find

import "testing"

func Test_finrdNumbers(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Example 1:",
			args: args{
				[]int{12, 345, 2, 6, 7896},
			},
			want: 2,
		},
		{
			name: "Example 2:",
			args: args{
				[]int{555, 901, 482, 1771},
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := finrdNumbers(tt.args.nums); got != tt.want {
				t.Errorf("finrdNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}
