package find

import "strconv"

// Runtime: 4 ms
// Memory Usage: 3.2 MB
func finrdNumbers(nums []int) int {
	sum := 0
	for _, v := range nums {
		t := strconv.Itoa(v)
		if len(t)%2 == 0 {
			sum++
		}
	}
	return sum
}
