package find

// 16.一群猴子排成一圈,按1,2,...,n 依次編號,然後從第1隻開始數,數到第m隻,把它踢出圈,從它後面再開始數,
// 再數到第m隻,在把它踢出去...如此不停的進行下去,直到最後剩下一隻猴子為止那隻就叫做大王,要求編程模擬此過程,
// 輸入m,n,輸出最後那個大王的編號.
func fundMonkeyKing(n, m int) int {
	monkey := make([]int, n)
	for i := 1; i <= n; i++ {
		monkey[i-1] = i
	}

	count := 0
	for n > 0 {
		count++
		if count == m {
			monkey = monkey[1:]
			count = 0
			n--
		} else {
			//移除第一個值
			front := monkey[0]
			monkey = monkey[1:]
			//把移除的第一個值,加在slice後面
			monkey = append(monkey, front)
		}
		if n == 1 {
			return monkey[0]
		}
	}
	return 0
}
