package find

import "testing"

func Test_fundMonkeyKing(t *testing.T) {
	type args struct {
		n int
		m int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Example 1:",
			args: args{n: 6, m: 2},
			want: 5,
		},
		{
			name: "Example 2:",
			args: args{n: 10, m: 5},
			want: 3,
		},
		{
			name: "Example 3:",
			args: args{n: 5, m: 12},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fundMonkeyKing(tt.args.n, tt.args.m); got != tt.want {
				t.Errorf("fundMonkeyKing() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_fundMonkeyKing(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fundMonkeyKing(6, 2)
	}
}
