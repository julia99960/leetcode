package duplicates

func removeDuplicates(nums []int) int {
	long := len(nums)
	if long <= 1 {
		return long
	}
	diff := 0
	for i := 1; i < long; i++ {
		if nums[diff] != nums[i] {
			diff++
			nums[diff] = nums[i]
		}
	}
	return diff + 1
}
