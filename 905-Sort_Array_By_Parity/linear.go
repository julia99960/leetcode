package linear

func sortArrayByParity(A []int) []int {
	b1 := []int{}
	b2 := []int{}

	for _, v := range A {
		if v%2 == 0 {
			b1 = append(b1, v)
		} else {
			b2 = append(b2, v)
		}
	}
	return append(b1, b2...)
}
