package squares

func sortedSquares(A []int) []int {
	l := len(A)
	right := 0
	for right < l && A[right] < 0 {
		right++
	}
	left := right - 1

	ans := make([]int, 0, l)
	for i := 0; i < l; i++ {
		if left < 0 {
			ans = append(ans, A[right]*A[right])
			right++
			continue
		} else if right >= l {
			ans = append(ans, A[left]*A[left])
			left--
			continue
		}

		if abs(A[left]) <= abs(A[right]) {
			ans = append(ans, A[left]*A[left])
			left--
		} else {
			ans = append(ans, A[right]*A[right])
			right++
		}
	}
	return ans
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
