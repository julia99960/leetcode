package main

import "fmt"

// 771. Jewels and Stones
// Runtime: 0 ms, faster than 100.00% of Go online submissions for Jewels and Stones.
// Memory Usage: 2.2 MB, less than 10.19% of Go online submissions for Jewels and Stones.
func numJewelsInStones(J string, S string) int {
	keyValue := make(map[string]bool)
	for _, v := range J {
		keyValue[string(v)] = true
	}

	count := 0
	for _, word := range S {
		if keyValue[string(word)] {
			count++
		}
	}
	return count
}

func main() {
	// 771. Jewels and Stones
	// You're given strings J representing the types of stones that are jewels, and S representing the stones you have.  Each character in S is a type of stone you have.  You want to know how many of the stones you have are also jewels.
	// The letters in J are guaranteed distinct, and all characters in J and S are letters. Letters are case sensitive, so "a" is considered a different type of stone from "A".
	J := "aA"
	S := "aAAbbbb"
	fmt.Println(numJewelsInStones(J, S))
}
