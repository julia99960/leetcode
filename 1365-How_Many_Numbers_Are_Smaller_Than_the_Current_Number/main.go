package main

import "fmt"

func smallerNumbersThanCurrent(nums []int) []int {
	res := make([]int, len(nums))
	for i := range nums {
		count := 0
		for j := range nums {
			if j != i && nums[i] > nums[j] {
				count++
			}
		}
		res[i] = count
	}
	return res
}

func main() {
	nums := []int{8, 1, 2, 2, 3}
	fmt.Println(smallerNumbersThanCurrent(nums))
}
