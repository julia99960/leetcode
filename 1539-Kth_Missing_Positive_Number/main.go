package main

import "fmt"

// 1539. Kth Missing Positive Number
// 輸入一個正整數等差數列(等差為1,在某些位置會缺少值))及一個正整數k
// arr = [2,3,4,7,11], k = 5
// 查找此數組中缺少的第k個正整數。
// output: 9
func findKthPositive(arr []int, k int) int {
	lostK := 0
	for index := range arr {
		if index == 0 && arr[0] >= 2 {
			for i := 1; i < arr[0]; i++ {
				lostK++
				if lostK == k {
					return i
				}
			}
		} else if index != 0 && arr[index]-arr[index-1] > 1 {
			for i := arr[index-1]; i <= arr[index]; i++ {
				if i != arr[index-1] && i != arr[index] {
					lostK++
					if lostK == k {
						return i
					}
				}
			}
		}
	}
	if lostK < k {
		return arr[len(arr)-1] + (k - lostK)
	}
	return 0
}

func main() {
	arr := []int{2, 3, 4, 7, 11}
	k := 5
	fmt.Println(findKthPositive(arr, k))
}
