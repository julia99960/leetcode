package main

import "fmt"

// 1603. Design Parking System
// Runtime: 36 ms, faster than 99.56% of Go online submissions for Design Parking System.
// Memory Usage: 6.7 MB, less than 94.71% of Go online submissions for Design Parking System.

// ParkingSystem 設定停車格規格
type ParkingSystem struct {
	Big    int
	Medium int
	Small  int
}

// Constructor 輸入停車格規格
func Constructor(big int, medium int, small int) ParkingSystem {
	p := ParkingSystem{
		Big:    big,
		Medium: medium,
		Small:  small,
	}
	return p
}

// AddCar 輸入車子的大小
func (th *ParkingSystem) AddCar(carType int) bool {
	switch carType {
	case 1: // 大車
		p := th.Big - 1
		th.Big = p
		if p < 0 {
			p = p + 1
			th.Big = p
			return false
		}
		return true
	case 2: // 中車
		m := th.Medium - 1
		th.Medium = m
		if m < 0 {
			m = m + 1
			th.Medium = m
			return false
		}
		return true
	case 3: // 小車
		s := th.Small - 1
		th.Small = s
		if s < 0 {
			s = s + 1
			th.Small = s
			return false
		}
		return true
	}
	return false
}

func main() {
	// 1603. Design Parking System
	// Input
	// ["ParkingSystem", "addCar", "addCar", "addCar", "addCar"]
	// [[1, 1, 0], [1], [2], [3], [1]]
	// Output
	// [null, true, true, false, false]
	p := ParkingSystem{1, 1, 0}
	t := ParkingSystem(p)
	fmt.Println(t.AddCar(1))
	fmt.Println(t.AddCar(2))
	fmt.Println(t.AddCar(3))
	fmt.Println(t.AddCar(1))
}
