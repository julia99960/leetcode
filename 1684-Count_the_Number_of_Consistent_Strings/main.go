package main

import (
	"fmt"
	"strings"
)

// 1684. Count the Number of Consistent Strings
// Runtime: 56 ms, faster than 15.79% of Go online submissions for Count the Number of Consistent Strings.
// Memory Usage: 6.8 MB, less than 7.89% of Go online submissions for Count the Number of Consistent Strings.
func countConsistentStrings(allowed string, words []string) int {
	ans := 0
	for _, element := range words {
		sum := 0
		for _, e := range strings.Split(element, "") {
			if !strings.ContainsAny(allowed, e) {
				sum++
			}
		}
		if sum == 0 {
			ans++
		}
	}
	fmt.Println(ans)
	return ans
}

func main() {
	// 1684. Count the Number of Consistent Strings
	// You are given a string allowed consisting of distinct characters and an array of strings words. A string is consistent if all characters in the string appear in the string allowed.
	// Return the number of consistent strings in the array words.
	// Input: allowed = "ab", words = ["ad","bd","aaab","baa","badab"]
	// Output: 2
	// Explanation: Strings "aaab" and "baa" are consistent since they only contain characters 'a' and 'b'.
	allowed := "ab"
	words := []string{"ad", "bd", "aaab", "baa", "badab"}
	fmt.Println(countConsistentStrings(allowed, words))
}
