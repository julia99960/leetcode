package decrypt

import "testing"

func Test_freqAlphabets(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "example 1:",
			args: args{
				"10#11#12",
			},
			want: "jkab",
		},
		{
			name: "example 2:",
			args: args{
				"1326#",
			},
			want: "acz",
		},
		{
			name: "example 3:",
			args: args{
				"25#",
			},
			want: "y",
		},
		{
			name: "example 4:",
			args: args{
				"12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#",
			},
			want: "abcdefghijklmnopqrstuvwxyz",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := freqAlphabets(tt.args.s); got != tt.want {
				t.Errorf("freqAlphabets() = %v, want %v", got, tt.want)
			}
		})
	}
}
