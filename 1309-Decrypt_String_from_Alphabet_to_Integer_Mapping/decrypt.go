package decrypt

// Runtime: 0 ms, faster than 100.00% of Go online submissions for Decrypt String from Alphabet to Integer Mapping.
func freqAlphabets(s string) string {
	var lowercase string
	long := len(s)
	c := 0
	for i, v := range s {
		if v == '#' {
			if c-2 > 0 {
				for j := i - c; j < i-2; j++ {
					lowercase += string(s[j] - 48 + 96)
				}
			}
			av := 96 + (s[i-2]-48)*10 + (s[i-1] - 48)
			lowercase += string(av)
			c = 0
		} else {
			c++
		}
	}

	if s[long-1] != '#' {
		for i := long - c; i < long; i++ {
			lowercase += string(s[i] - 48 + 96)
		}
	}
	return lowercase
}
