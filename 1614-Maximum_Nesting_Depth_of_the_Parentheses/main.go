package main

func maxDepth(s string) int {
	depth, maxDepth := 0, 0
	for _, v := range s {
		if v == 40 {
			depth++
			if depth > maxDepth {
				maxDepth = depth
			}
		}
		if v == 41 {
			depth--
		}
	}
	return maxDepth
}

func main() {
	s := "1+(2*3)/(2-1)"
	maxDepth(s)
}
