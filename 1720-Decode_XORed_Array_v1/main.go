package main

import (
	"math"
)

// Runtime: 324 ms, faster than 5.56% of Go online submissions for Decode XORed Array.
// Memory Usage: 8 MB, less than 6.35% of Go online submissions for Decode XORed Array.

// 十進位轉二進位(反過來看)
func int2(n int) (a []int) {
	for n > 0 {
		if n%2 == 0 {
			a = append(a, 0)
			n = n / 2
		} else {
			a = append(a, 1)
			n = (n - 1) / 2
			if n == 0 {
				a = append(a, 0)
			}
		}
	}
	return a
}

func int10(n []int) int {
	a := 0
	for i, k := range n {
		t := int(math.Pow(2, float64(i)))
		a += k * t
	}
	return a
}

func reVerXOR(a []int, c []int) (ans int) {
	if len(a) > len(c) {
		diff := len(a) - len(c)
		for i := 0; i < diff; i++ {
			c = append(c, 0)
		}
	}
	if len(a) < len(c) {
		diff := len(c) - len(a)
		for i := 0; i < diff; i++ {
			a = append(a, 0)
		}
	}
	b := []int{}
	for i, k := range a {
		if k == 1 && c[i] == 1 {
			b = append(b, 0)
		}
		if k == 1 && c[i] == 0 {
			b = append(b, 1)
		}
		if k == 0 && c[i] == 0 {
			b = append(b, 0)
		}
		if k == 0 && c[i] == 1 {
			b = append(b, 1)
		}
	}
	return int10(b)
}

// 當兩兩數值相同為否，而數值不同時為真
func decode(encoded []int, first int) []int {
	a := []int{first}
	for _, v := range encoded {
		n := reVerXOR(int2(first), int2(v))
		first = n
		a = append(a, n)
	}
	return a
}

func main() {
	e := []int{6, 2, 7, 3}
	f := 4
	decode(e, f)
}
