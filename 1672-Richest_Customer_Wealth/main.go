package main

import "fmt"

// 1672. Richest Customer Wealth
// Runtime: 4 ms, faster than 96.74% of Go online submissions for Richest Customer Wealth.
// Memory Usage: 3.1 MB, less than 100.00% of Go online submissions for Richest Customer Wealth.
func maximumWealth(accounts [][]int) int {
	sumMax := 0
	for _, element := range accounts {
		sum := 0
		for i := 0; i < len(element); i++ {
			sum += element[i]
		}
		if sum > sumMax {
			sumMax = sum
		}
	}
	return sumMax
}
func main() {
	// 1672. Richest Customer Wealth
	// https://leetcode.com/problems/richest-customer-wealth/
	accounts := [][]int{{1, 2, 3}, {3, 2, 1}}
	fmt.Println(maximumWealth(accounts)) // 6
}
