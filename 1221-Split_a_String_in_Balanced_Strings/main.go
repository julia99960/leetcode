package main

func balancedStringSplit(s string) int {
	a := 0
	b := 0
	for _, k := range s {
		if k == int32(82) {
			a++
		}
		if k == int32(76) {
			a--
		}
		if a == 0 {
			b++
		}
	}
	return b
}

func main() {
	s := "RLRRLLRLRL"
	balancedStringSplit(s)
}
