package sum

import (
	"testing"
)

func Test_sumZero(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Example 1:",
			args: args{
				5,
			},
		},
		{
			name: "Example 2:",
			args: args{
				3,
			},
		},
		{
			name: "Example 3:",
			args: args{
				1,
			},
		},
		{
			name: "Example 4:",
			args: args{
				4,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := sumZero(tt.args.n)
			a := 0
			for _, v := range got {
				a += v
			}
			if len(got) == tt.args.n && 0 != a {
				t.Errorf("sumZero() = %v", got)
			}
		})
	}
}
