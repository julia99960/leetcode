package sum

func sumZero(n int) []int {
	sum := make([]int, n)
	m := 0
	skip := true
	if n%2 == 0 {
		m = n / 2
		skip = false
	} else {
		m = (n - 1) / 2
	}
	for i := 1; i <= m; i++ {
		if !skip && i == m {
			sum[0] = i
			sum[n-1] = -i
		} else {
			sum[2*i-1] = -i
			sum[2*i] = i
		}
	}

	return sum
}
