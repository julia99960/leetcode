package surface

// 90 / 90 test cases passed.
// Status: Accepted
// Runtime: 4 ms
// Memory Usage: 3.7 MB
func surfaceArea(grid [][]int) int {
	n := len(grid)
	if n == 1 {
		return 2 + 4*grid[0][0]
	}

	sum := 0
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			v := grid[i][j]

			// 上有值
			if i-1 >= 0 {
				if grid[i-1][j] < v {
					sum += v - grid[i-1][j]
				}
			} else { // 上沒有值
				sum += v
			}

			// 右有值
			if j+1 < n {
				if grid[i][j+1] < v {
					sum += v - grid[i][j+1]
				}
			} else {
				sum += v
			}

			// 下有值
			if i+1 < n {
				if grid[i+1][j] < v {
					sum += v - grid[i+1][j]
				}
			} else {
				sum += v
			}

			// 左有值
			if j-1 >= 0 {
				if grid[i][j-1] < v {
					sum += v - grid[i][j-1]
				}
			} else {
				sum += v
			}

			// 頂部 && 底部
			if v > 0 {
				sum += 2
			}
		}
	}

	return sum
}
