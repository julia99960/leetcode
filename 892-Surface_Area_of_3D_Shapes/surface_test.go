package surface

import "testing"

func Test_surfaceArea(t *testing.T) {
	type args struct {
		grid [][]int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Example 1:",
			args: args{
				[][]int{{2}},
			},
			want: 10,
		},
		{
			name: "Example 2:",
			args: args{
				[][]int{{1, 2}, {3, 4}},
			},
			want: 34,
		},
		{
			name: "Example 3:",
			args: args{
				[][]int{{1, 0}, {0, 2}},
			},
			want: 16,
		},
		{
			name: "Example 4:",
			args: args{
				[][]int{{1, 1, 1}, {1, 0, 1}, {1, 1, 1}},
			},
			want: 32,
		},
		{
			name: "Example 5:",
			args: args{
				[][]int{{2, 2, 2}, {2, 1, 2}, {2, 2, 2}},
			},
			want: 46,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := surfaceArea(tt.args.grid); got != tt.want {
				t.Errorf("surfaceArea() = %v, want %v", got, tt.want)
			}
		})
	}
}
