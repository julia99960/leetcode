package shuffle

// 53 / 53 test cases passed.
// Runtime: 4 ms
// Memory Usage: 3.7 MB
func shuffle(nums []int, n int) []int {
	new := make([]int, 2*n)
	for i := 0; i < n; i++ {
		new[2*i] = nums[i]
		new[2*i+1] = nums[n+i]
	}
	return new
}
