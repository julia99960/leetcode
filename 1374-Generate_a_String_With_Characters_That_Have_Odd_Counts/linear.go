package linear

func generateTheString(n int) string {
	s := ""
	if n%2 == 0 {
		for i := 0; i < n-1; i++ {
			s += "a"
		}
		s += "b"
	} else {
		for i := 0; i < n; i++ {
			s += "a"
		}
	}

	return s
}
