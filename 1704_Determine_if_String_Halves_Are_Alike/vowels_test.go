package vowels

import "testing"

func Test_halvesAreAlike(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Example 1:",
			args: args{
				"book",
			},
			want: true,
		},
		{
			name: "Example 2:",
			args: args{
				"textbook",
			},
			want: false,
		},
		{
			name: "Example 3:",
			args: args{
				"MerryChristmas",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := halvesAreAlike(tt.args.s); got != tt.want {
				t.Errorf("halvesAreAlike() = %v, want %v", got, tt.want)
			}
		})
	}
}
