package vowels

// Runtime: 0 ms, faster than 100.00% of Go online submissions for Determine if String Halves Are Alike.
func halvesAreAlike(s string) bool {
	halves := len(s) / 2
	count := 0
	for i, v := range s {
		if i < halves {
			if v == 97 || v == 101 || v == 105 || v == 111 || v == 117 || v == 65 || v == 69 || v == 73 || v == 79 || v == 85 {
				count++
			}
		} else {
			if v == 97 || v == 101 || v == 105 || v == 111 || v == 117 || v == 65 || v == 69 || v == 73 || v == 79 || v == 85 {
				count--
			}
		}
	}
	return count == 0
}
