package main

import (
	"fmt"
	"strings"
)

// 1678. Goal Parser Interpretation
// Runtime: 0 ms, faster than 100.00% of Go online submissions for Goal Parser Interpretation.
// Memory Usage: 2 MB, less than 100.00% of Go online submissions for Goal Parser Interpretation.
func interpret(command string) string {
	a := strings.ReplaceAll(command, "()", "o")
	p := strings.ReplaceAll(a, "(al)", "al")
	return p
}

func main() {
	// 1678. Goal Parser Interpretation
	// You own a Goal Parser that can interpret a string command. The command consists of an alphabet of "G", "()" and/or "(al)" in some order. The Goal Parser will interpret "G" as the string "G", "()" as the string "o", and "(al)" as the string "al". The interpreted strings are then concatenated in the original order.
	// Given the string command, return the Goal Parser's interpretation of command.
	// Input: command = "G()(al)"
	// Output: "Goal"
	// Explanation: The Goal Parser interprets the command as follows:
	// G -> G
	// () -> o
	// (al) -> al
	// The final concatenated result is "Goal".
	command := "G()(al)"
	fmt.Println(interpret(command))
}
